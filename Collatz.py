#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------
import math
from typing import IO, List
from MetaCache import meta_cache


def round_up_1000(num: int) -> int:
    """
    rounds up the beginning of the range to the nearest 1000
    num an int
    return an int of the rounded number
    """
    return int(math.ceil(num / 1000.0)) * 1000


def round_down_1000(num: int) -> int:
    """
    rounds down the end of the range to the nearest 1000
    num an int
    return an int of the rounded number
    """
    return int(math.floor(num / 1000.0)) * 1000


def cycle_length(n: int) -> int:
    """
    computes the cycle length for a given int (Collatz)
    n an int
    return an int of the cycle length of the given int
    """
    assert n > 0
    c = 1
    while n > 1:
        if (n % 2) == 0:
            n = n // 2
        else:
            n = (3 * n) + 1
        c += 1
    assert c > 0
    return c


# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    if j < i:  # swaps beginning and end of range if end is less than beginning
        i, j = j, i

    if j - i < 1000:  # if input range is less than range used for meta cache (1000)
        ans = -1
        for x in range(i, j + 1):
            ans = max(ans, cycle_length(x))
        return ans

    rounded_i = round_up_1000(i)
    rounded_j = round_down_1000(j)

    # need to iterate over the 1000s in between and get the overall max cycle length
    ans = -1
    num = rounded_i

    while num < rounded_j:
        ans = max(ans, meta_cache[num // 1000])
        num += 1000

    for x in range(i, rounded_i):  # compute cycle lengths above i and below rounded_i
        ans = max(ans, cycle_length(x))

    for x in range(
        rounded_j, j + 1  # pylint: disable=bad-continuation
    ):  # compute cycle lengths above rounded_j and below j
        ans = max(ans, cycle_length(x))

    return ans


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
